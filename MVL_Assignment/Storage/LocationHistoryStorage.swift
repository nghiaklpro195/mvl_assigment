//
//  HistoryStorage.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

protocol LocationHistoryStorage {
    func add(item: QueryInfoResult)
    func removeAll()
    func all() -> [QueryInfoResult]
}






//
//  MemoryLocationHistoryStorage.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

final class MemoryLocationHistoryStorage: LocationHistoryStorage {
    private var data = [QueryInfoResult]()
    
    func add(item: QueryInfoResult) {
        data.append(item)
    }
    
    func removeAll() {
        data.removeAll()
    }
    
    func all() -> [QueryInfoResult] {
        return data
    }
}

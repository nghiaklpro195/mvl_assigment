//
//  Config.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation

enum Config {
    enum Key: String {
        case GoogleMapAPIKey = "AIzaSyBYd-DYICmc0elQg2Qxg1JGtBYXAC2GSL4"
        case AirQualityAPIToken = "6437e20ab650e1000dfa83854f4a9c2d4f1b4da8"
    }
}

extension Config.Key {
    var value: String {
        return self.rawValue
    }
}

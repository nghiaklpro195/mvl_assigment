//
//  LocationResultInfo.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

struct LocationResultInfo {
    let coordidateInfo: LocationInfo
    let name: String
}

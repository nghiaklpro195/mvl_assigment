//
//  PointType.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

enum PointType {
    case pointA
    case pointB
}

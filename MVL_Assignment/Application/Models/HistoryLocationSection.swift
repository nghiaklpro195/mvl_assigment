//
//  HistoryLocationSection.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxDataSources

struct HistoryLocationSection {
    var items: [QueryInfoResult]
}

extension HistoryLocationSection:  RxDataSources.SectionModelType {
    init(original: HistoryLocationSection, items: [QueryInfoResult]) {
        self = original
        self.items = items
    }
}

//
//  LocationInfo.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

struct LocationInfo {
    let lat: Double
    let long: Double
}

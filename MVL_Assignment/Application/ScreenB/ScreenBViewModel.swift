//
//  ScreenBViewModel.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxSwift
import CoreLocation
import Resolver
import RxDataSources

enum NavigationType {
    case moveNext(pointA: LocationInfo, pointB: LocationInfo)
    case back
}

class ScreenBViewModel: ViewModelType {
    struct Input {
        let selectCoordiate: Observable<CLLocationCoordinate2D>
    }
    
    struct Output {
        let currentLocationStream: Observable<CLLocationCoordinate2D>
        let selectPointResult: Observable<Void>
        let navigation: Observable<NavigationType>
        let historySection: Observable<[HistoryLocationSection]>
    }
    
    private let pointType: PointType
    @Injected var locationService: LocationService
    @Injected var sharedDataService: PointDataService
    @Injected var historyService: LocationHistoryStorage
    
    init(pointType: PointType) {
        self.pointType = pointType
    }
    
    func transform(input: Input) -> Output {
        locationService.runService()
        
        
        let selectResult = input.selectCoordiate.flatMapLatest { [weak self] data -> Observable<Void> in
            guard let self = self else {
                return Observable.empty()
            }
            
            
            let locationInfo = LocationInfo(lat: data.latitude, long: data.longitude)
            switch self.pointType {
            case .pointA:
                self.sharedDataService.updatePointA(locationInfo)
            case .pointB:
                self.sharedDataService.updatePointB(locationInfo)
            }
            
            return Observable.just(())
        }
        
        let combineStream = Observable.combineLatest(
            sharedDataService.pointAStream,
            sharedDataService.pointBStream
        )
        
        let navigation = input.selectCoordiate.withLatestFrom(combineStream).map { (pointA, pointB) -> NavigationType in
            if pointA != nil && pointB != nil {
                return NavigationType.moveNext(pointA: pointA!, pointB: pointB!)
            } else {
                return NavigationType.back
            }
        }

        
        return Output(
            currentLocationStream: self.locationService.currentLocationStream.observe(on: MainScheduler.instance),
            selectPointResult: selectResult.observe(on: MainScheduler.instance),
            navigation: navigation.observe(on: MainScheduler.instance),
            historySection: Observable.just([HistoryLocationSection(items: historyService.all())]).observe(on: MainScheduler.instance)
        )
    }
    
    
}

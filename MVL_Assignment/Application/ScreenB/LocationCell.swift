//
//  LocationCell.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import UIKit
import RxSwift
import Reusable
import SnapKit

final class LocationCell: UITableViewCell, Reusable {
    private let containerView = UIView()
    private let nameLabel = UILabel()
    private let latLabel = UILabel()
    private let longLabel = UILabel()
    private let airLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    var info: QueryInfoResult? {
        didSet {
            guard let data = info else {
                return
            }
            
            nameLabel.text = data.locationResultInfo.name
            latLabel.text = "Lat: \(data.locationResultInfo.coordidateInfo.lat)"
            longLabel.text = "Long: \(data.locationResultInfo.coordidateInfo.long)"
            airLabel.text = "Air: \(data.air)"
            
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        selectionStyle = .none
        contentView.addSubview(containerView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(latLabel)
        containerView.addSubview(longLabel)
        containerView.addSubview(airLabel)
        
        contentView.backgroundColor = .white
        containerView.layer.borderColor =  UIColor.gray.cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 5
        
        containerView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(6)
            make.trailing.equalToSuperview().offset(-6)
            make.top.equalToSuperview().offset(3)
            make.bottom.equalToSuperview().offset(-3)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(3)
            make.trailing.equalToSuperview().offset(-3)
            make.top.equalToSuperview().offset(3)
        }
        
        latLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(3)
            make.trailing.equalToSuperview().offset(-3)
            make.top.equalTo(nameLabel.snp.bottom).offset(3)
        }
        
        longLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(3)
            make.trailing.equalToSuperview().offset(-3)
            make.top.equalTo(latLabel.snp.bottom).offset(3)
        }
        
        airLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(3)
            make.trailing.equalToSuperview().offset(-3)
            make.top.equalTo(longLabel.snp.bottom).offset(3)
            make.bottom.equalToSuperview()
        }
    
    }
}

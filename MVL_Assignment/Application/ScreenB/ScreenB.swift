//
//  ScreenB.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import UIKit
import RxSwift
import RxCocoa
import CoreLocation
import MapKit
import GoogleMaps
import SnapKit
import RxDataSources

final class ScreenB: UIView, ScreenViewable {
    
    private let bag = DisposeBag()
    
    weak var navStack: NavigationStack?
    
    private let mapView = GMSMapView()
    private let marker = GMSMarker()
    
    private let bottomView = UIView()
    private let topView = UIView()
    private let backButton = UIButton()
    
    private let selectButon = UIButton()
    private let tableView = UITableView()
    
    private let currentCoordiateSubject = BehaviorRelay<CLLocationCoordinate2D?>(value: nil)
    
    private let dataSource = RxTableViewSectionedReloadDataSource<HistoryLocationSection> { ds, tableView, indexPath, item in
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: LocationCell.self)
        cell.info = item
        return cell
    }
    
    private let viewModel: ScreenBViewModel
    init(viewModel: ScreenBViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func attach(on view: UIView) {
        view.addSubview(self)
        addSubview(mapView)
        addSubview(bottomView)
        addSubview(topView)
        
        bottomView.addSubview(tableView)
        bottomView.addSubview(selectButon)
        
        topView.addSubview(backButton)
    }
    
    func setupConstraint() {
        snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        mapView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.bottom.equalToSuperview().offset(-24)
            make.height.equalToSuperview().multipliedBy(0.3)
        }
        
        selectButon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.bottom.equalToSuperview().offset(-12)
            make.height.equalTo(50)
        }
        
        tableView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.top.equalToSuperview().offset(12)
            make.bottom.equalTo(selectButon.snp.top).offset(-12)
        }
        
        topView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.top.equalToSuperview().offset(44)
            make.height.equalTo(60)
        }
        
        backButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(6)
            make.width.height.equalTo(60)
            make.centerY.equalToSuperview()
        }
    }
    
    func visualize() {
        
        backgroundColor = .white
        bottomView.backgroundColor = .white
        bottomView.layer.cornerRadius = 10
        
        topView.backgroundColor = .white
        topView.layer.cornerRadius = 10
        
        backButton.setImage(UIImage(named: "Back"), for: .normal)
        
        mapView.camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 1)
        mapView.isMyLocationEnabled = false
        mapView.mapType = .normal
        mapView.delegate = self
        
        tableView.backgroundColor = .white
        tableView.register(cellType: LocationCell.self)
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = true
        
        selectButon.backgroundColor = .blue
        selectButon.setTitle("Select Point", for: .normal)
        selectButon.setTitleColor(.white, for: .normal)
        
        
    }
    
    func detach() {
        self.removeFromSuperview()
    }
    
    func setupRx() {
        
        let output = viewModel.transform(input: ScreenBViewModel.Input(
            selectCoordiate: selectButon.rx.tap.withLatestFrom(currentCoordiateSubject.asObservable().compactMap { $0 })
        ))
        
        output.historySection
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            guard let self = self else {
                return
            }
            
            let item = self.dataSource[indexPath]
            let coordidate = item.locationResultInfo.coordidateInfo
            
            let position = CLLocationCoordinate2D(latitude: coordidate.lat, longitude: coordidate.long)
            
            self.marker.position = position
            self.mapView.camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude: position.longitude, zoom: 15)
        })
        .disposed(by: bag)
        
        backButton.rx.tap
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.navStack?.pop()
            })
            .disposed(by: bag)
        
        output.currentLocationStream
            .compactMap { $0 }
            .take(1)
            .asObservable()
            .subscribe(onNext: { [weak self] locValue in
                guard let self = self else {
                    return
                }
            
                self.mapView.camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 15)
            
                let position = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
                self.marker.position = position
                self.marker.title = "Current Locationr"
                self.marker.map = self.mapView
                self.currentCoordiateSubject.accept(locValue)
                
        }).disposed(by: self.bag)
        
        output.selectPointResult.subscribe(onNext: { _ in
            
        })
        .disposed(by: bag)
        
        output.navigation.subscribe(onNext: { [weak self] navType in
            guard let self = self else {
                return
            }
            
            switch navType {
            case .back:
                self.navStack?.pop()
            case .moveNext(let pointA, let pointB):
                let screenC = ScreenC(viewModel: ScreenCViewModel(pointAInfo: pointA, pointBInfo: pointB))
                self.navStack?.push(viewable: screenC)
            }
            
        })
        .disposed(by: bag)
        
    }
    
    var rootView: UIView {
        return self
    }
    
    func setNavStack(_ navStack: NavigationStack) {
        self.navStack = navStack
    }
    
    deinit {
        print("Deinit Screen B")
    }

}


extension ScreenB: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("MoveView \(gesture)")
        if gesture {
            bottomView.isHidden = true
            topView.isHidden = true
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        bottomView.isHidden = false
        topView.isHidden = false
    }
  
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print(#function)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.marker.position = position.target
        self.currentCoordiateSubject.accept(position.target)
       
    }
    
    
}

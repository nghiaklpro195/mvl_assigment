//
//  Screenable.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//


import UIKit
import RxSwift

protocol ScreenViewable {
    func attach(on view: UIView)
    func setupConstraint()
    func visualize()
    func detach()
    func setupRx()
    
    var navStack: NavigationStack? { get }
    func setNavStack(_ navStack: NavigationStack)
    
    var rootView: UIView { get }
    
    
   
}



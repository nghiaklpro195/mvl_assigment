//
//  NavigationStack.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import UIKit

final class NavigationStack {
    private var stack = [ScreenViewable]()
    
    private var rootVC: UIViewController!
    
    func runOn(vc: UIViewController) {
        self.rootVC = vc
    }
    
    func push(viewable: ScreenViewable) {
        stack.append(viewable)
        
        viewable.attach(on: self.rootVC.view)
        viewable.visualize()
        viewable.setupConstraint()
        viewable.setupRx()
        viewable.setNavStack(self)
        
        print(stack.count)
    }
    
    func setRoot(view: ScreenViewable) {
        stack.forEach {  $0.detach() }
        stack.removeAll()
        
        push(viewable: view)
    }
    
    func pop() {
        guard let topViewable = stack.popLast() else {
            return
        }
        
        topViewable.detach()
    }
    
    func popToRoot() {
        print(#function)
        while stack.count != 1 {
            let view = stack.popLast()
            view?.detach()
        }
    }
}

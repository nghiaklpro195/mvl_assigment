//
//  ScreenAViewModel.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxSwift
import Resolver

class ScreenAViewModel: ViewModelType {
    
    struct Input {
        let setPointAction: Observable<PointType>
        let resetAction: Observable<Void>
    }
    
    
    struct Output {
        let navigateToSettingPoint: Observable<PointType>
        let pointAStream: Observable<LocationInfo?>
        let pointBStream: Observable<LocationInfo?>
        let resetResult: Observable<Void>
    }
    
    @Injected var sharedDataService: PointDataService
    @Injected var locationService: LocationService
    
    
    func transform(input: Input) -> Output {
        locationService.requestPemissionIfNeeded()
        
        let resetResult = input.resetAction.flatMapLatest { [weak self] _ -> Observable<Void> in
            guard let self = self else {
                return Observable.empty()
            }
            
            self.sharedDataService.updatePointA(nil)
            self.sharedDataService.updatePointB(nil)
            
            return Observable.just(())
        }
        
        return Output(
            navigateToSettingPoint: input.setPointAction.observe(on: MainScheduler.instance),
            pointAStream: sharedDataService.pointAStream.observe(on: MainScheduler.instance),
            pointBStream: sharedDataService.pointBStream.observe(on: MainScheduler.instance),
            resetResult: resetResult.observe(on: MainScheduler.instance)
        )
    }
}



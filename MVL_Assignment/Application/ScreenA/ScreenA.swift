//
//  ScreenA.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

final class ScreenA: UIView, ScreenViewable {
    weak var navStack: NavigationStack?

    private let bag = DisposeBag()

    private let pointAButton = UIButton()
    private let pointALabel = UILabel()
    private let pointAContent = UILabel()
    
    private let pointBButton = UIButton()
    private let pointBLabel = UILabel()
    private let pointBContent = UILabel()
    
    private let resetButton = UIButton()
    

    var rootView: UIView {
        return self
    }
    
    let viewModel: ScreenAViewModel
    
    init(viewModel: ScreenAViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func attach(on view: UIView) {
        
        backgroundColor = .white
        
        view.addSubview(self)
        addSubview(pointALabel)
        addSubview(pointBLabel)
        addSubview(pointAContent)
        addSubview(pointBContent)
        
        
        addSubview(pointAButton)
        addSubview(pointBButton)
        
        addSubview(resetButton)
    }
    
    
    func visualize() {
        pointAButton.setTitle("Set Point A", for: .normal)
        pointAButton.backgroundColor = .blue
        pointAButton.setTitleColor(.white, for: .normal)
        
        pointBButton.setTitle("Set Point B", for: .normal)
        pointBButton.backgroundColor = .blue
        pointBButton.setTitleColor(.white, for: .normal)
        
        resetButton.setTitle("Reset", for: .normal)
        resetButton.backgroundColor = .red
        resetButton.setTitleColor(.white, for: .normal)
        
        pointALabel.text = "Point A:"
        pointALabel.textColor = .black
        pointALabel.numberOfLines = 1
        
        pointAContent.text = "No data"
        pointAContent.textColor = .black
        pointAContent.font = UIFont.systemFont(ofSize: 12)
        
        pointBLabel.text = "Point B:"
        pointBLabel.textColor = .black
        pointBLabel.numberOfLines = 1
        
        pointBContent.font = UIFont.systemFont(ofSize: 12)
        pointBContent.text = "No data"
        pointBContent.textColor = .black
        
    }
    
    func setupRx() {
        let setPointAAction = pointAButton.rx.tap.map { PointType.pointA }
        let setPointBAction = pointBButton.rx.tap.map { PointType.pointB }
        
        let output = viewModel.transform(input: ScreenAViewModel.Input(
            setPointAction: Observable.merge(setPointAAction, setPointBAction),
            resetAction: resetButton.rx.tap.map { _ in  }
        ))
        
        output.resetResult.subscribe(onNext: { [weak self] in
            guard let self = self else {
                return
            }
            print("Reset")
        })
        .disposed(by: self.bag)
        
        
        output.pointAStream.map { info in
            print("setData")
            guard let data = info else {
                return "No data"
            }

            return "\(data.lat), \(data.long)"
        }
        .bind(to: pointAContent.rx.text)
        .disposed(by: bag)

        output.pointBStream.map { info in
            guard let data = info else {
                return "No data"
            }

            return "\(data.lat), \(data.long)"
        }
        .bind(to: pointBContent.rx.text)
        .disposed(by: bag)
        
        output.navigateToSettingPoint.subscribe(onNext: { [weak self] pointType in
            guard let self = self else {
                return
            }
            
            let screenB = ScreenB(viewModel: ScreenBViewModel(pointType: pointType))
            self.navStack?.push(viewable: screenB)
                                  
        })
        .disposed(by: bag)
        
        
    }
    
    func setupConstraint() {
        snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        pointAButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.centerY.equalToSuperview()
            make.height.equalTo(50)
        }
        
        pointALabel.snp.makeConstraints { make in
            make.leading.equalTo(pointAButton)
            make.top.equalTo(pointAButton.snp.bottom).offset(6)
        }
        
        pointAContent.snp.makeConstraints { make in
            make.trailing.equalTo(pointAButton)
            make.top.equalTo(pointAButton.snp.bottom).offset(6)
        }
        
        pointBButton.snp.makeConstraints { make in
            make.leading.trailing.height.equalTo(pointAButton)
            make.top.equalTo(pointAContent.snp.bottom).offset(10)
        }
        
        pointBLabel.snp.makeConstraints { make in
            make.leading.equalTo(pointAButton)
            make.top.equalTo(pointBButton.snp.bottom).offset(6)
        }
        
        pointBContent.snp.makeConstraints { make in
            make.trailing.equalTo(pointAButton)
            make.top.equalTo(pointBButton.snp.bottom).offset(6)
        }
        
        resetButton.snp.makeConstraints { make in
            make.leading.trailing.height.equalTo(pointAButton)
            make.top.equalTo(pointBContent.snp.bottom).offset(20)
        }
    }
    
    func detach() {
        self.removeFromSuperview()
    }
    
    func setNavStack(_ navStack: NavigationStack) {
        self.navStack = navStack
    }
    
}

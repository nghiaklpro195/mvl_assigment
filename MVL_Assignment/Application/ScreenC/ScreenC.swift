//
//  ScreenC.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import UIKit
import RxSwift
import RxCocoa

final class ScreenC: UIView, ScreenViewable {
    weak var navStack: NavigationStack?
    
    private let bag = DisposeBag()
    
    func setNavStack(_ navStack: NavigationStack) {
        self.navStack = navStack
    }
    
    private let pointALabel = UILabel()
    private let pointBLabel = UILabel()
    
    private let pointACoordinateLabel = UILabel()
    private let pointBCoordinateLabel = UILabel()
    
    private let pointANameLabel = UILabel()
    private let pointBNameLabel = UILabel()
    
    private let pointAAirQualityLabel = UILabel()
    private let pointBAirQualityLabel = UILabel()
    
    private let backButton = UIButton()
    
    private let viewModel: ScreenCViewModel
    
    init(viewModel: ScreenCViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func attach(on view: UIView) {
        view.addSubview(self)
        addSubview(pointALabel)
        addSubview(pointBLabel)
        addSubview(pointACoordinateLabel)
        addSubview(pointANameLabel)
        addSubview(pointAAirQualityLabel)
        
        addSubview(pointBCoordinateLabel)
        addSubview(pointBNameLabel)
        addSubview(pointBAirQualityLabel)
        
        addSubview(backButton)
    }
    
    func setupConstraint() {
        snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        pointALabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(64)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointACoordinateLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointALabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointANameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointACoordinateLabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointAAirQualityLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointANameLabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        
        pointBLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointAAirQualityLabel.snp.bottom).offset(24)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointBCoordinateLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointBLabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointBNameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointBCoordinateLabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        pointBAirQualityLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalTo(pointBNameLabel.snp.bottom).offset(6)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        
        backButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.height.equalTo(50)
            make.bottom.equalToSuperview().offset(-24)
        }
        
    }
    
    func visualize() {
        backgroundColor = .white
        [
            pointALabel,
            pointACoordinateLabel,
            pointANameLabel,
            pointAAirQualityLabel,
            
            pointBLabel,
            pointBCoordinateLabel,
            pointBNameLabel,
            pointBAirQualityLabel
        ].forEach {
            $0.textColor = .black
            $0.textAlignment = .left
            $0.numberOfLines = 0
        }
        
        pointALabel.text = "Point A Info"
        pointBLabel.text = "Point B Info"
        
        pointACoordinateLabel.text = "Coord: "
        pointBCoordinateLabel.text = "Coord: "
        
        pointANameLabel.text = "Name: "
        pointBNameLabel.text = "Name: "
        
        pointAAirQualityLabel.text = "Air: "
        pointBAirQualityLabel.text = "Air: "
        
        
        backButton.backgroundColor = .blue
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        
    }
    
    func detach() {
        self.removeFromSuperview()
    }
    
    func setupRx() {
        let ouput = viewModel.transform(input: ScreenCViewModel.Input())
        
        backButton.rx.tap.subscribe(onNext: { [weak self] _ in
            guard let self = self else {
                return
            }
            
            self.navStack?.popToRoot()
        })
        .disposed(by: bag)
        
        ouput.pointAInfoStream.subscribe(onNext: { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let response):
                let location = response.locationResultInfo
                self.pointACoordinateLabel.text = "Coord: \(location.coordidateInfo.lat), \(location.coordidateInfo.long)"
                self.pointANameLabel.text = "Name: \(location.name)"
                self.pointAAirQualityLabel.text = "Air: \(response.air)"
            case .failure(let error):
                print(error.localizedDescription)
            }
        }).disposed(by: self.bag)
        
        ouput.pointBInfoStream.subscribe(onNext: { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let response):
                let location = response.locationResultInfo
                self.pointBCoordinateLabel.text = "Coord: \(location.coordidateInfo.lat), \(location.coordidateInfo.long)"
                self.pointBNameLabel.text = "Name: \(location.name)"
                self.pointBAirQualityLabel.text = "Air: \(response.air)"
            case .failure(let error):
                print(error.localizedDescription)
            }
        }).disposed(by: self.bag)
        
    }
    
    deinit {
        print("Deinit Screen C")
    }
    
    var rootView: UIView {
        return self
    }
}


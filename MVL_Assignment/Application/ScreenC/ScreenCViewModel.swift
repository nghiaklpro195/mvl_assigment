//
//  ScreenCViewModel.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxSwift
import Resolver
import Alamofire



class ScreenCViewModel: ViewModelType {
    
    struct Input {
        
    }
    
    struct Output {
        let pointAInfoStream: Observable<Result<QueryInfoResult, Error>>
        let pointBInfoStream: Observable<Result<QueryInfoResult, Error>>
    }
    
    @Injected var sharedDataService: PointDataService
    @Injected var apiService: ApiService
    @Injected var queryService: QueryInfoService
    
    
    private let pointAInfo: LocationInfo
    private let pointBInfo: LocationInfo
    
    init(pointAInfo: LocationInfo, pointBInfo: LocationInfo) {
        self.pointAInfo = pointAInfo
        self.pointBInfo = pointBInfo
    }
    
    func transform(input: Input) -> Output {
        
        let pointAQueryInfo = queryService.query(with: pointAInfo).map { Result.success($0) }
        .catch { error -> Observable<Result<QueryInfoResult, Error>> in
            return Observable.just(Result.failure(error))
        }
        
        let pointBQueryInfo = queryService.query(with: pointBInfo).map { Result.success($0) }
        .catch { error -> Observable<Result<QueryInfoResult, Error>> in
            return Observable.just(Result.failure(error))
        }
        
        return Output(
            pointAInfoStream: pointAQueryInfo.observe(on: MainScheduler.instance),
            pointBInfoStream: pointBQueryInfo.observe(on: MainScheduler.instance)
        )
    }
    
    
}

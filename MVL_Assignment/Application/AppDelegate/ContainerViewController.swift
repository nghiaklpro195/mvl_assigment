//
//  ViewController.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import UIKit

final class ContainerViewController: UIViewController {
    private var navStack = NavigationStack()
    
    private let screenA = ScreenA(viewModel: ScreenAViewModel())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
        navStack.runOn(vc: self)
        navStack.setRoot(view: screenA)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}


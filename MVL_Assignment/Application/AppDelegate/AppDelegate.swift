//
//  AppDelegate.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import UIKit
import GoogleMaps
import Resolver

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(Config.Key.GoogleMapAPIKey.value)
        
        Resolver.registerAllServices()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = ContainerViewController()
        
        window?.makeKeyAndVisible()
        
        return true
    }
}

extension Resolver: ResolverRegistering {
  public static func registerAllServices() {
      register { MemoryLocationHistoryStorage() }.implements(LocationHistoryStorage.self).scope(.application)
      register { SharedPointDataService() }.implements(PointDataService.self).scope(.application)
      register { LocationService() }
      register { ApiService() }
      register { QueryInfoService() }
      register { MemoryQueryInfoCache() }.implements(QueryInfoCache.self).scope(.application)
  }
}


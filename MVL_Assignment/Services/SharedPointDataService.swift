//
//  SharedPointDataService.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxSwift
import RxRelay

protocol PointDataService {
    var pointAStream: Observable<LocationInfo?> { get }
    var pointBStream: Observable<LocationInfo?> { get }
    
    func updatePointA(_ info: LocationInfo?)
    func updatePointB(_ info: LocationInfo?)
}

class SharedPointDataService: PointDataService {
    
    var pointAStream: Observable<LocationInfo?> {
        return self.pointASubject.asObservable()
    }
    
    var pointBStream: Observable<LocationInfo?> {
        return self.pointBSubject.asObservable()
    }
    
    func updatePointA(_ info: LocationInfo?) {
        pointASubject.accept(info)
    }
    
    func updatePointB(_ info: LocationInfo?) {
        pointBSubject.accept(info)
    }
    
    private let pointASubject = BehaviorRelay<LocationInfo?>(value: nil)
    private let pointBSubject = BehaviorRelay<LocationInfo?>(value: nil)
}

//
//  QueryInfoService.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import RxSwift
import Resolver

final class QueryInfoService {
    
    @Injected var apiService: ApiService
    @Injected var historyStorage: LocationHistoryStorage
    @Injected var cacheService: QueryInfoCache
    
    func query(with locationInfo: LocationInfo) -> Observable<QueryInfoResult> {
        
        let lat = Int(locationInfo.lat)
        let long = Int(locationInfo.long)
        
        let key = "\(lat)_\(long)"

        if let getValue = cacheService.query(key: key) {
            return Observable.just(getValue)
        }
        
        let locationResult = apiService.request(api: GeoTargetApiType.getLocationInfo(
            latitude: locationInfo.lat,
            longitude: locationInfo.long,
            localityLanguage: "en"), R: LocationInfoResponse.self)
            .asObservable()
            .flatMapLatest({ data -> Observable<LocationResultInfo> in
                let location = LocationInfo(lat: data.latitude, long: data.longitude)
                let listCity = data.localityInfo.administrative.sorted { $0.order < $1.order }.suffix(2)
                
                let result = LocationResultInfo(
                    coordidateInfo: location,
                    name: listCity.map { $0.name }.joined(separator: ","))
                return Observable.just(result)
            })
         
        
        let airQualityResult = apiService.request(api: AirQualityApiType.getAirQuality(
            latitude: locationInfo.lat,
            longitude: locationInfo.long,
            token: Config.Key.AirQualityAPIToken.value), R: AirQualityResponse.self)
            .asObservable()
            .compactMap { $0.data }
            .map { $0.aqi }
            
        return Observable.zip(locationResult, airQualityResult).map { (location, airQuality) in
            return QueryInfoResult(air: airQuality, locationResultInfo: location)
        }
        .do { [weak self] value in
            let lat = Int(value.locationResultInfo.coordidateInfo.lat)
            let long = Int(value.locationResultInfo.coordidateInfo.long)
            let key = "\(lat)_\(long)"
            self?.cacheService.addCache(key: key, value: value)
            self?.historyStorage.add(item: value)
        }
            
    }
}

//
//  LocationService.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation
import CoreLocation
import RxSwift
import RxRelay

final class LocationService: NSObject {
    private let locationManager = CLLocationManager()
    
    private let currentCoordinateSubject = BehaviorRelay<CLLocationCoordinate2D?>(value: nil)
    
    var currentLocationStream: Observable<CLLocationCoordinate2D> {
        return currentCoordinateSubject.compactMap { $0 }
    }
    
    
    func requestPemissionIfNeeded() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        
    }

    func runService() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopService() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            return
            
        }
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        self.currentCoordinateSubject.accept(locValue)
    }
}

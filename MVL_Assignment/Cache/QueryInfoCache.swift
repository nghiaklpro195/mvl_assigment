//
//  QueryInfoCache.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

protocol QueryInfoCache {
    func query(key: String) -> QueryInfoResult?
    func addCache(key: String, value: QueryInfoResult)
    func flushCache()
}



//
//  MemoryQueryInfoCache.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 04/11/2021.
//

import Foundation

class MemoryQueryInfoCache: QueryInfoCache {
    private var dict = [String: QueryInfoResult]()
    
    func query(key: String) -> QueryInfoResult? {
        return dict[key]
    }
    
    func addCache(key: String, value: QueryInfoResult) {
        dict[key] = value
    }
    
    func flushCache() {
        dict.removeAll()
    }
    
    
}

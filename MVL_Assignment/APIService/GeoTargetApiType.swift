//
//  GeoTargetApiType.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation
import Alamofire

enum GeoTargetApiType {
    case getLocationInfo(latitude: Double, longitude: Double, localityLanguage: String)
}


extension GeoTargetApiType: TargetApiType {
    var baseURL: URL {
        return URL(string: "https://api.bigdatacloud.net")!
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var path: String {
        switch self {
        case .getLocationInfo:
            return "/data/reverse-geocode-client"
        }
    }
    
    var methodType: HTTPMethod {
        switch self {
        case .getLocationInfo:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getLocationInfo(let latitude, let longitude, let localityLanguage):
            return [
                "latitude": latitude,
                "longitude": longitude,
                "localityLanguage": localityLanguage
            ]
        }
    }
    
    var encoder: ParameterEncoding {
        switch self {
        case .getLocationInfo:
            return URLEncoding.queryString
        }
    }
}

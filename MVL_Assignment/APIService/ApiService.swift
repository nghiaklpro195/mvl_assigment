//
//  ApiService.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation
import Alamofire
import RxSwift

enum MVLAppError: Error {
    case cantCastResponse
}

extension MVLAppError: LocalizedError {
    
}

class ApiService {
    func request<R: Decodable>(api: TargetApiType, R: R.Type) -> Single<R> {
        return Single<R>.create { observer in
            let request = AF.request(api.baseURL.appendingPathComponent(api.path),
                       method: api.methodType,
                       parameters: api.parameters,
                       encoding: api.encoder,
                       headers: HTTPHeaders(api.headers ?? [:]),
                       interceptor: nil,
                       requestModifier: nil)
            
            request
                .cURLDescription { description in
                    print(description)
                }
                .response { result in
                if let err = request.error {
                    observer(.failure(err))
                    return
                }
                
                guard let data = result.data else {
                    return
                }
                
                let jsonDecoder = JSONDecoder()
                guard let model = try? jsonDecoder.decode(R.self, from: data) else {
                    observer(.failure(MVLAppError.cantCastResponse))
                    return
                }
                
                observer(.success(model))
                
            }
              
            
            return Disposables.create {
                if !request.isCancelled {
                    request.cancel()
                }
            }

        }.observe(on: SerialDispatchQueueScheduler(qos: .background))
        
    }
}

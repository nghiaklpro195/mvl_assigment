//
//  TargetApiType.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation
import Alamofire

enum MethodType: String {
    case GET
    case POST
}

protocol TargetApiType {
    var baseURL: URL { get }
    var headers: [String: String]? { get }
    var path: String { get }
    var methodType: HTTPMethod { get }
    var parameters: [String: Any]? { get }
    var encoder: ParameterEncoding { get }
}

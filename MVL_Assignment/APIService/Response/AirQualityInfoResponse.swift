//
//  AirQualityInfoResponse.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation

struct AirQualityResponse: Codable {
    let status: String
    let message: String?
    let data: AirQualityResponseData?
}

struct AirQualityResponseData: Codable {
    let aqi: Int
    let idx: Int
}

//
//  LocationInfoResponse.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation

// MARK: - Welcome
struct LocationInfoResponse: Codable {
    let latitude: Double
    let longitude: Double
    let lookupSource: String
    let plusCode: String
    let localityLanguageRequested: String
    let continent: String
    let continentCode: String
    let countryName: String
    let countryCode: String
    let principalSubdivision: String
    let principalSubdivisionCode: String
    let city: String
    let locality: String
    let postcode: String
    let localityInfo: LocalityInfo
    
//    enum CodingKeys: String, CodingKey {
//        case localityInfo = "localityInfo"
//    }
}

struct LocalityInfo: Codable {
    let administrative: [Info]
    let informative: [Info]
}

struct Info: Codable {
    let order: Int
    let adminLevel: Int?
    let name: String
    let ativeDescription: String?
    let isoName: String?
    let isoCode: String?
    let wikidataID: String?
    let geonameID: Int?

    enum CodingKeys: String, CodingKey {
        case order = "order"
        case adminLevel = "adminLevel"
        case name = "name"
        case ativeDescription = "description"
        case isoName = "isoName"
        case isoCode = "isoCode"
        case wikidataID = "wikidataId"
        case geonameID = "geonameId"
    }
}

//
//  AirQualityApiType.swift
//  MVL_Assignment
//
//  Created by Nghia Nguyen on 03/11/2021.
//

import Foundation
import Alamofire

enum AirQualityApiType {
    case getAirQuality(latitude: Double, longitude: Double, token: String)
}

extension AirQualityApiType: TargetApiType {
    var baseURL: URL {
        return URL(string: "https://api.waqi.info")!
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var path: String {
        switch self {
        case .getAirQuality(let lat, let long, _):
            return "/feed/geo:\(lat);\(long)/"
        }
    }
    
    var methodType: HTTPMethod {
        switch self {
        case .getAirQuality:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getAirQuality(_, _, let token):
            return [
                "token": token
            ]
        }
    }
    
    var encoder: ParameterEncoding {
        switch self {
        case .getAirQuality:
            return URLEncoding.queryString
        }
    }
}
